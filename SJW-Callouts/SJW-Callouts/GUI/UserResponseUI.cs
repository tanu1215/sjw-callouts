﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

using Rage;

using RAGENativeUI;
using RAGENativeUI.Elements;

internal class UserResponseGUI
{
    private UIMenu responseMenu;

    private UIMenuColoredItem positiveResponseColoredItem;
    private UIMenuColoredItem negativeResponseColoredItem;

    private MenuPool menuPool;

    internal bool IsPositiveResponse { get; private set; }
    internal bool IsVisible { get; private set; }

    internal UserResponseGUI()
    {
        menuPool = new MenuPool();

        responseMenu = new UIMenu("Responses", "Officer response.");

        menuPool.Add(responseMenu);

        responseMenu.OnItemSelect += OnItemSelect;

        positiveResponseColoredItem = new UIMenuColoredItem("Positive Response", Color.Green, Color.White);
        negativeResponseColoredItem = new UIMenuColoredItem("Negative Response", Color.Red, Color.White);
    }

    internal void Draw()
    {
        responseMenu.Visible = true;
        IsVisible = true;

        AddControlToMenu(positiveResponseColoredItem);
        AddControlToMenu(negativeResponseColoredItem);

        new GameFiber(() =>
        {
            while (true)
            {
                menuPool.ProcessMenus();
            }
        }, "ResponseMenuGameFiber");
    }

    private void AddControlToMenu(UIMenuItem i)
    {
        responseMenu.AddItem(i);
    }

    private void OnItemSelect(UIMenu sender, UIMenuItem selectedItem, int index)
    {
        if (selectedItem == positiveResponseColoredItem)
        {
            IsPositiveResponse = true;
        }
        responseMenu.Clear();
        responseMenu.Visible = false;
        IsVisible = false;
    }
}