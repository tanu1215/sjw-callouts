﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Rage;

namespace SJW_Callouts.Utils
{
    internal static class Logger
    {
        private static string prefix = "[SJW Callouts]";

        internal static void Log(string message)
        {
           Game.LogTrivial(string.Format("{0} {1}", prefix, message));
        }
    }
}