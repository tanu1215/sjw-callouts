﻿using System;
using System.Windows.Forms;
using Rage;

namespace SJW_Callouts.Utils
{
    internal class Config
    {
        private static Config s;
        private static readonly object syncLock = new object();

        private Config()
        {

        }

        internal static Config GetConfig()
        {
            if (s == null)
            {
                lock(syncLock)
                {
                    if (s == null)
                    {
                        s = new Config();
                    }
                }
            }

            return s;
        }

        private InitializationFile ini;

        private KeysConverter kc = new KeysConverter();

        private const string CALLOUT_CATEGORY = "[Callouts]";

        internal bool CapitalistPigEnabled => ini.ReadBoolean(CALLOUT_CATEGORY, "Capitalist-Pig", true);

        internal bool FeministFemaleOfficerEnabled => ini.ReadBoolean(CALLOUT_CATEGORY, "Feminist-Female-Officer", true);

        private string GetTalkKey() { return ini.ReadString(CALLOUT_CATEGORY, "TalkKey", "Y"); }

        internal Keys TalkKey => (Keys)(kc.ConvertFromString(GetTalkKey()));

        private string GetYesKey() { return ini.ReadString(CALLOUT_CATEGORY, "YesKey", "Y"); }

        internal Keys YesKey => (Keys)(kc.ConvertFromString(GetYesKey()));

        private string GetNoKey() { return ini.ReadString(CALLOUT_CATEGORY, "NoKey", "N"); }

        internal Keys NoKey => (Keys)(kc.ConvertFromString(GetNoKey()));

        internal void Create(string path)
        {
            ini = new InitializationFile(path);

            if (!ini.Exists())
            {
                try
                {
                    ini.Create();
                    Logger.Log("Ini Created!");
                }

                catch (Exception e)
                {
                    Logger.Log(string.Format("Error generating INI! {0}", e));
                    Game.DisplayNotification("Error creating INI file...");
                    Game.DisplayNotification("Please send the log file to tanu1215 or BlockBa5her!");
                }
            }
        }
    }
}
