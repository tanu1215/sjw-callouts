﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rage;

namespace SJW_Callouts.Utils
{
    internal static class Common
    {
        internal static Player Player => Game.LocalPlayer;
        internal static Ped Character => Game.LocalPlayer.Character;

        internal static Random random = new Random();

        internal static int GetRandomNumber(int lowerBound, int upperBound)
        {
            return random.Next(lowerBound, upperBound);
        }
    }
}
