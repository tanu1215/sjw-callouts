﻿using System.Linq;
using System.Windows.Forms;
using LSPD_First_Response.Mod.Callouts;
using Rage;
using SJW_Callouts.Utils;
using SJW_Callouts.GUI;


namespace SJW_Callouts.Callouts
{
    [CalloutInfo("Capitalist Pig", CalloutProbability.Medium)]
    internal class CapitalistPig : Callout
    {
        //Our social justice warrior.
        private Ped sjw;
       
        //Our local player.
        private Ped player = Common.Character;

        //Our callout location.
        private Vector3 calloutLocation;

        //Blip representing our callout location.
        private Blip locationBlip;

        public override bool OnBeforeCalloutDisplayed()
        {
            calloutLocation = World.GetNextPositionOnStreet(player.Position.Around(350f));

            //If the peds are valid, display the area that the callout is in.
            ShowCalloutAreaBlipBeforeAccepting(calloutLocation, 15f);
            AddMinimumDistanceCheck(5f, calloutLocation);

            CalloutMessage = "Capitalist Pig";
            CalloutPosition = calloutLocation;

            return base.OnBeforeCalloutDisplayed();
        }

        public override bool OnCalloutAccepted()
        {
            sjw = new Ped(calloutLocation);
            sjw.BlockPermanentEvents = true;

            sjw.Tasks.Cower(-1);

            locationBlip = sjw.AttachBlip();

            Game.DisplaySubtitle("Respond to the capitalist pig!", 6000);

            return base.OnCalloutAccepted();
        }

        public override void OnCalloutNotAccepted()
        {
            base.OnCalloutNotAccepted();
        }

        public override void Process()
        {
            base.Process();

            GameFiber.StartNew(() =>
            {
                while (player.DistanceTo(sjw.Position) >= 10f)
                {
                    GameFiber.Yield();
                }

                Game.DisplayHelp("Press Y to advance conversation.");

                string[] messages = new string[3];
                messages[0] = "Caller: HELP ME!!!!!";
                messages[1] = "Caller: A FREAKIN CAPITALIST JUST TRIED TO SELL ME SOMETHING!!!!";
                messages[2] = "Caller: PLEASE ARREST THEM!!";

                foreach (string msg in messages)
                {
                    while (!Game.IsKeyDownRightNow(Keys.Y))
                    {
                        GameFiber.Yield();
                    }
                    Game.DisplaySubtitle(msg);
                }

                Game.DisplaySubtitle("Press Y to respond positively, n for negatively.", 3000);

                Keys choice = Keys.None;

                while (choice == Keys.None)
                {
                    if (Game.IsKeyDownRightNow(Keys.Y))
                    {
                        choice = Keys.Y;
                    }

                    else if (Game.IsKeyDownRightNow(Keys.N))
                    {
                        choice = Keys.N;
                    }
                }

                //TODO ADD MORE INDEPTH CHOICES W/ MENU.
                string[] positiveResponses =
                {
                    "Okay, I'll look into it.", "WHAT?! SOCIALISM MUST PREVAIL!",
                    "Where's Bernie Sanders when we need him!"
                };

                string[] negativeResponses =
                {
                    "Is this a joke?", "Why are you so triggered?",
                    "This is not a crime."
                };

                if (choice == Keys.Y)
                {
                    Game.DisplaySubtitle(positiveResponses[Common.GetRandomNumber(0, positiveResponses.Count() - 1)]);
                }

                else 
                {
                    Game.DisplaySubtitle(negativeResponses[Common.GetRandomNumber(0, negativeResponses.Count() - 1)]);
                }
            });
        }

        public override void End()
        {
            base.End();
        }
    }
}