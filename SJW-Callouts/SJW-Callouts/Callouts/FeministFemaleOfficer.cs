﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;
using LSPD_First_Response.Mod.Callouts;
using SJW_Callouts.Utils;
using LSPD_First_Response.Mod.API;
using Rage;

namespace SJW_Callouts.Callouts
{
    [CalloutInfo("FeministFemaleOfficer", CalloutProbability.Medium)]
    public class FeministFemaleOfficer : Callout
    {
        //Prefix Properties
        private string fPrefix = "~g~FEMINIST~w~: ";
        private string cPrefix = "~y~Me~w~: ";
        private string vPrefix = "~r~Victim~w~: ";

        //Rage Properties
        private Vector3 spawnPoint;
        private Ped c = Common.Character;
        private Ped feminist;
        private Ped victim;
        private Blip calloutBlip;
        public string[] dialog { get; private set; }

        //Boolean Properties
        private bool playerOnScene;
        private bool playerOnDialog;
        private bool playerAfterDialog;

        //Pursuit Properties
        private LHandle pursuit;
        private bool pursuitCreated;
        private bool pursuitNeverCreated;

        public override bool OnBeforeCalloutDisplayed()
        {
            //Switch to set dialog for game
            switch(Convert.ToInt32(c.IsMale))
            {
                case 0:
                    {
                        //Setting the female dialog
                        dialog = new string[]
                        {
                            fPrefix + "Oh my god officer!",
                            fPrefix + "You arrived so fast, thank you!",
                            fPrefix + "This person infront of me was talking SHIT",
                            cPrefix + "What was the person rudely saying, ma'am?",
                            fPrefix + "He was saying that he should have sex with this woman,",
                            fPrefix + "That is discrimination against women",
                            cPrefix + "Can I ask how that is discrimination?",
                            fPrefix + "Yes I can officer,",
                            fPrefix + "Why not have sex with men, also he is treating women like an object!",
                            vPrefix + "This woman is lying, I never said anything like that,",
                            vPrefix + "and I even appoligized to you!",
                            fPrefix + "YOU NEVER APPOLIGIZED TO ME!",
                            cPrefix + "Be quiet you two! Calm down for a second while I get my information",
                            cPrefix + "Ok, ma'am, so this person has freedom of speech,",
                            fPrefix + "*interupts* BUT IT'S SLANDER!",
                            vPrefix + "HOW IS THAT SLANDER, I WAS JUST WALKING!",
                            cPrefix + "Ok, from what I can tell,",
                            cPrefix + "this woman right here doesn't understand freedom of speech",
                            fPrefix + "What do you mean, its slander, sexism.",
                            cPrefix + "Ma'am, if you continue, I will arrest you for disorderly conduct",
                            fPrefix + "Ok, I'll continue then",
                            cPrefix + "You can't just continue, you know!",
                            fPrefix + "I don't care what you say"
                        };

                        break;
                    }
                case 1:
                    {
                        //Setting the male dialog
                        dialog = new string[]
                        {
                            fPrefix + "Ugh, why didn't they send a female officer like I asked",
                            fPrefix + "You dirty pigs never listen to what women have to say",
                            cPrefix + "Ma'am, I'm just doing my job. They told me to respond,",
                            cPrefix + "So I responded. Lets resolve the situation, OK?",
                            fPrefix + "*Sigh* fine",
                            cPrefix + "So, sir, tell me what happened here.",
                            fPrefix + "WHY DOES THE MALE HAVE TO GO FIRST?",
                            cPrefix + "Be quiet ma'am, need to get information first.",
                            vPrefix + "Well, I was talking to my buddy about how I want to marry this girl,",
                            vPrefix + "And then this woman came out of nowhere and said,",
                            vPrefix + "That I was treating women as objects.",
                            fPrefix + "HE IS LYING, STOP LYING YOU SLANDERER",
                            cPrefix + "Ma'am, the next time you blurt out, I will arrest you",
                            fPrefix + "For what, that man is lying!",
                            cPrefix + "For disorderly conduct, all the people around can hear you!",
                            cPrefix + "Ma'am, just be quiet while I am investigating",
                            cPrefix + "Alright sir, continue",
                            vPrefix + "After she said that to me, I told her to 'Fuck off'",
                            vPrefix + "She then got really angry with me and tried to hit me",
                            vPrefix + "So I walked off, thinking nothing of it, just some crazy lady",
                            vPrefix + "And she started to follow me, and thats when I called 911",
                            fPrefix + "I NEVER FOLLOWED HIM!",
                            cPrefix + "Ma'am, remember what I told you before!",
                            fPrefix + "I DON'T CARE WHAT YOU SAID BEFORE"
                        };

                        break;
                    }
                default:
                    {
                        //Issued when cannot detect if player is male or female
                        Game.DisplayHelp("BUG: Couldn't read if player was male of female");

                        dialog = new string[]
                        {
                            fPrefix + "Ugh, why didn't they send a female officer like I asked",
                            fPrefix + "You dirty pigs never listen to what women have to say",
                            cPrefix + "Ma'am, I'm just doing my job. They told me to respond,",
                            cPrefix + "So I responded. Lets resolve the situation, OK?",
                            fPrefix + "*Sigh* fine",
                            cPrefix + "So, sir, tell me what happened here.",
                            fPrefix + "WHY DOES THE MALE HAVE TO GO FIRST?",
                            cPrefix + "Be quiet ma'am, need to get information first.",
                            vPrefix + "Well, I was talking to my buddy about how I want to marry this girl,",
                            vPrefix + "And then this woman came out of nowhere and said,",
                            vPrefix + "That I was treating women as objects.",
                            fPrefix + "HE IS LYING, STOP LYING YOU SLANDERER",
                            cPrefix + "Ma'am, the next time you blurt out, I will arrest you",
                            fPrefix + "For what, that man is lying!",
                            cPrefix + "For disorderly conduct, all the people around can hear you!",
                            cPrefix + "Ma'am, just be quiet while I am investigating",
                            cPrefix + "Alright sir, continue",
                            vPrefix + "After she said that to me, I told her to 'Fuck off'",
                            vPrefix + "She then got really angry with me and tried to hit me",
                            vPrefix + "So I walked off, thinking nothing of it, just some crazy lady",
                            vPrefix + "And she started to follow me, and thats when I called 911",
                            fPrefix + "I NEVER FOLLOWED HIM!",
                            cPrefix + "Ma'am, remember what I told you before!",
                            fPrefix + "I DON'T CARE WHAT YOU SAID BEFORE"
                        };

                        break;
                    }
            }

            //Setting spawn point
            spawnPoint = World.GetNextPositionOnStreet(c.Position.Around(500f, 1000f));

            //Setting callout information
            CalloutMessage = "Triggered Feminist";
            CalloutPosition = feminist.Position;
            AddMinimumDistanceCheck(100f, CalloutPosition);
            ShowCalloutAreaBlipBeforeAccepting(CalloutPosition, 50f);

            return base.OnBeforeCalloutDisplayed();
        }

        public override bool OnCalloutAccepted()
        {
            //Creating the peds
            feminist = new Ped(spawnPoint);
            victim = new Ped(feminist.Position.Around(2f));

            //Settings ped information
            feminist.IsPersistent = true;
            feminist.BlockPermanentEvents = true;
            victim.IsPersistent = true;
            victim.BlockPermanentEvents = true;

            //Attaching blip to feminist Ped
            calloutBlip = feminist.AttachBlip();
            calloutBlip.Color = Color.Yellow;
            calloutBlip.IsRouteEnabled = true;
            calloutBlip.Name = "Callout Position";

            //Giving peds tasks
            victim.Tasks.Wander();
            feminist.Tasks.FollowToOffsetFromEntity(victim, victim.GetOffsetPositionFront(-5));

            return base.OnCalloutAccepted();
        }

        public override void OnCalloutNotAccepted()
        {
            base.OnCalloutNotAccepted();
        }

        public override void Process()
        {
            base.Process();
            
            //Clearing tasks and checking if the player is arriving on scene
            if (c.DistanceTo(calloutBlip.Position) < 30f && !playerOnScene)
            {
                if (!feminist.Exists() | !victim.Exists())
                {
                    AbortCallout();
                }
            
                //Clearing the tasks and setting the heading of peds
                feminist.Tasks.Clear();
                victim.Tasks.Clear();
                victim.Heading = feminist.Heading + 180f;

                //Setting blip settings
                
                
                //Setting boolean
                playerOnScene = true;
            }

            //Dialog part to where if the player is near the 2 suspects
            if (c.DistanceTo(calloutBlip.Position) < 5f && !playerOnDialog)
            {   
                playerOnDialog = true;
                Game.DisplayHelp("Press ~y~Y~w~ to talk to the people");
            
                GameFiber.StartNew(() =>
                {
                    //Initial dialog
                    foreach(string msg in dialog)
                    {
                        while (!Game.IsKeyDown(Config.TalkKey) | c.DistanceTo(calloutBlip.Position) > 10f)
                        {
                            GameFiber.Yield();

                            if (c.DistanceTo(calloutBlip.Position) > 10f)
                            {
                                Game.DisplayHelp("Go back to the ~r~CALLOUT~w~ Location");
                            }
                        }

                        if (!feminist.Exists() | !victim.Exists())
                        {
                            //Aborting the callout if feminist and/or victim is killed
                            AbortCallout();
                        }

                        //Saying the dialog
                        Game.DisplaySubtitle(msg);

                        GameFiber.Sleep(500);
                    }
                }, "SJW Callouts Dialog Thread");
                
                playerAfterDialog = true;
            }
            
            if (playerAfterDialog && !pursuitCreated)
            {
                new GameFiber(() =>
                {
                    Keys choice;

                    Game.DisplayHelp(string.Format("Press {0} to arrest or {1} to let her go", Config.YesKey.ToString(), Config.NoKey.ToString()));

                    while (true)
                    {
                        GameFiber.Yield();

                        if (Game.IsKeyDown(Config.YesKey))
                        {
                            choice = Keys.Y;
                            break;
                        }

                        if (Game.IsKeyDown(Config.NoKey))
                        {
                            choice = Keys.N;
                            break;
                        }
                    }

                    if (choice == Keys.Y)
                    {
                        string[] response = new string[]
                        {

                        };

                        foreach(string msg in response)
                        {
                            while (!Game.IsKeyDown(Keys.Y) | c.DistanceTo(calloutBlip.Position) > 10f)
                            {
                                GameFiber.Yield();

                                if (c.DistanceTo(calloutBlip.Position) > 10f)
                                {
                                    Game.DisplayHelp("Go back to the ~r~CALLOUT~w~ Location");
                                }
                            }

                            Game.DisplaySubtitle(msg);

                            GameFiber.Sleep(500);
                        }

                        //Tasks for the feminist
                        feminist.Tasks.Clear();
                        feminist.Tasks.FightAgainst(c);

                        //Creating the pursuit
                        pursuit = Functions.CreatePursuit();
                        Functions.AddPedToPursuit(pursuit, feminist);
                        Functions.SetPursuitIsActiveForPlayer(pursuit, true);
                        pursuitCreated = true;

                        //Creating a new GameFiber
                        new GameFiber(new ThreadStart(WaitForWeaponPullout));
                    }

                    if (choice == Keys.N)
                    {
                        string[] response = new string[]
                        {
                            
                        };

                        foreach(string msg in response)
                        {
                            while (!Game.IsKeyDown(Config.TalkKey) | c.DistanceTo(calloutBlip.Position) > 10f)
                            {
                                GameFiber.Yield();

                                if (c.DistanceTo(calloutBlip.Position) > 10f)
                                {
                                    Game.DisplayHelp("Go back to the ~r~CALLOUT~w~ Location");
                                }
                            }

                            Game.DisplaySubtitle(msg);

                            GameFiber.Sleep(500);
                        }

                        feminist.Tasks.Clear();
                        feminist.Tasks.Wander();

                        //Setting pursuit is true for the end.
                        pursuitNeverCreated = true;
                    }
                }, "SJW Callouts");
            }
            
            //Ending the callout
            if (!Functions.IsPursuitStillRunning(pursuit) && pursuitCreated | pursuitNeverCreated)
            {
                End();
            }
        }

        public override void End()
        {
            base.End();

            //Testing to see if Entities exist and then deleting them
            if (feminist.Exists())
            {
                feminist.Delete();
            }
            if (victim.Exists())
            {
                victim.Delete();
            }
            if (calloutBlip.Exists())
            {
                calloutBlip.Delete();
            }
        }

        /// <summary>
        /// Void to abort the callout if anything goes wrong
        /// </summary>
        private void AbortCallout()
        {
            if (Functions.IsPursuitStillRunning(pursuit))
            {
                Functions.ForceEndPursuit(pursuit);
            }
            if (feminist.Exists())
            {
                feminist.Delete();
            }
            if (victim.Exists())
            {
                victim.Delete();
            }
            if (calloutBlip.Exists())
            {
                calloutBlip.Delete();
            }
            Functions.StopCurrentCallout();
        }

        private void WaitForWeaponPullout()
        {
            //Waiting for equipped weapon
            while (c.Inventory.EquippedWeapon == null)
            {
                GameFiber.Yield();
            }

            //Initiating tasks
            feminist.Tasks.Clear();
            feminist.Tasks.ReactAndFlee(feminist);

            WomanChaseLines();
        }

        private void WomanChaseLines()
        {
            //Saying the lines for the feminist
            Game.DisplaySubtitle(fPrefix + "OH SHIT HE GOT A WEAPON");

            DateTime dt = DateTime.Now.AddSeconds(10);
            while (dt > DateTime.Now)
            {
                if (!Functions.IsPursuitStillRunning(pursuit) && pursuitCreated)
                {
                    break;
                }
            }

            Game.DisplaySubtitle(fPrefix + "THIS IS RAPE, SOMEONE CALL 911");
            GameFiber.Sleep(1000);
            Game.DisplaySubtitle(cPrefix + "Ma'am, I am 911!");
        }
    }
}