﻿using System;
using LSPD_First_Response.Mod.API;
using SJW_Callouts.Utils;

namespace SJW_Callouts
{
    public class Main : Plugin
    {
        /// <summary>
        /// Initialize file for LSPDFR, so when it is loaded, this will load
        /// </summary>
        public override void Initialize()
        {
            Logger.Log("=====================");
            Logger.Log("Loading...");
            Logger.Log("Version 1 developed by BlockBa5her and Tanu1215");
            Logger.Log("=====================");

            Functions.OnOnDutyStateChanged += OnOnDutyStateChangedHandler;

            string path = "plugins/lspdfr/sjw-callouts.ini";
            Config.Create(path);
        }

        /// <summary>
        /// Nothing here :)
        /// </summary>
        public override void Finally()
        {
            Logger.Log("Finalizing");
        }

        /// <summary>
        /// When the Duty state is changed then it will call this
        /// </summary>
        /// <param name="OnDuty"></param>
        private void OnOnDutyStateChangedHandler(bool OnDuty)
        {
            //If going OnDuty and not OffDuty then it will register callouts
            if (OnDuty)
            {
                RegisterCallouts();
            }
        }

        /// <summary>
        /// This is where you register callouts to the game.
        /// </summary>
        private void RegisterCallouts()
        {
            Functions.RegisterCallout(typeof(Callouts.CapitalistPig));
            Functions.RegisterCallout(typeof(Callouts.FeministFemaleOfficer));
        }
    }
}
